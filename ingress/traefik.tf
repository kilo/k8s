# Install traefik as an ingress controller
resource "helm_release" "traefik" {
  name = "traefik"

  repository = "https://traefik.github.io/charts"
  chart      = "traefik"
  version    = "25.0.0"

  namespace        = "traefik"
  create_namespace = true

  values = [
    <<EOF
certResolvers:
  letsencrypt:
    caServer: https://acme-v02.api.letsencrypt.org/directory
    email: "sysmans@tardisproject.uk"
    tlsChallenge: true
    storage: "/data/acme.json"
    dnsChallenge:
      provider: pdns

envFrom:
- secretRef:
    name: pdns-api-info

podSecurityContext:
  fsGroup: 65532

ingressRoute:
  dashboard:
    enabled: true
    middlewares:
      - name: traefik-dashboard-auth

extraObjects:
  - apiVersion: traefik.containo.us/v1alpha1
    kind: Middleware
    metadata:
      name: traefik-dashboard-auth
    spec:
      basicAuth:
        secret: traefik-dashboard-auth-secret

additionalArguments:
  - "--providers.http.endpoint=https://console.tardisproject.uk/traefik"
  - "--providers.http.pollInterval=5m"
  - "--entrypoints.web.http.redirections.entryPoint.priority=10"

persistence:
  enabled: true
  storageClass: openebs-hostpath

ports:
  web:
    redirectTo:
      port: websecure
      priority: 10
EOF
  ]
}
