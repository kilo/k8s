resource "kubernetes_manifest" "clusterpolicy_auto_tls" {
  manifest = {
    "apiVersion" = "kyverno.io/v1"
    "kind"       = "ClusterPolicy"
    "metadata" = {
      "name" = "auto-tls"
    }
    "spec" = {
      "rules" = [
        {
          "match" = {
            "any" = [
              {
                "resources" = {
                  "kinds" = [
                    "Ingress",
                  ]
                }
              },
            ]
          }
          "mutate" = {
            "patchStrategicMerge" = {
              "metadata" = {
                "annotations" = {
                  "traefik.ingress.kubernetes.io/router.tls"              = "true"
                  "traefik.ingress.kubernetes.io/router.tls.certresolver" = "letsencrypt"
                }
              }
            }
          }
          "name" = "auto-tls"
        },
      ]
    }
  }
}
