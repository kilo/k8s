# Miscellaneous validation for ingress things.
# All of these only apply to user namespaces (ones with tardisproject.uk/owner set)

locals {
  namespace_selector = {
    "matchLabels" = {
      "tardisproject.uk/owner" = "*"
    }
  }
}

resource "kubernetes_manifest" "clusterpolicy_restrict_ingress_host_wildcards" {
  manifest = {
    "apiVersion" = "kyverno.io/v1"
    "kind"       = "ClusterPolicy"
    "metadata" = {
      "annotations" = {
        "kyverno.io/kubernetes-version"   = "1.23"
        "kyverno.io/kyverno-version"      = "1.6.2"
        "policies.kyverno.io/category"    = "Other"
        "policies.kyverno.io/description" = "Ingress hosts optionally accept a wildcard as an alternative to precise matching. In some cases, this may be too permissive as it would direct unintended traffic to the given Ingress resource. This policy enforces that any Ingress host does not contain a wildcard character."
        "policies.kyverno.io/minversion"  = "1.6.0"
        "policies.kyverno.io/severity"    = "medium"
        "policies.kyverno.io/subject"     = "Ingress"
        "policies.kyverno.io/title"       = "Restrict Ingress Host with Wildcards"
      }
      "name" = "restrict-ingress-wildcard"
    }
    "spec" = {
      "rules" = [
        {
          "match" = {
            "any" = [
              {
                "resources" = {
                  "kinds" = [
                    "Ingress",
                  ]
                  "namespaceSelector" = local.namespace_selector
                }
              },
            ]
          }
          "name" = "block-ingress-wildcard"
          "preconditions" = {
            "all" = [
              {
                "key"      = "{{ request.operation || 'BACKGROUND' }}"
                "operator" = "AnyIn"
                "value" = [
                  "CREATE",
                  "UPDATE",
                ]
              },
            ]
          }
          "validate" = {
            "foreach" = [
              {
                "deny" = {
                  "conditions" = {
                    "any" = [
                      {
                        "key"      = "{{ contains(element.host, '*') }}"
                        "operator" = "Equals"
                        "value"    = true
                      },
                    ]
                  }
                }
                "list" = "request.object.spec.rules"
              },
            ]
            "message" = "Wildcards are not permitted as hosts."
          }
        },
      ]
      "validationFailureAction" = "Enforce"
    }
  }
}
resource "kubernetes_manifest" "clusterpolicy_unique_ingress_host_path" {
  manifest = {
    "apiVersion" = "kyverno.io/v1"
    "kind"       = "ClusterPolicy"
    "metadata" = {
      "annotations" = {
        "kyverno.io/kubernetes-version"   = "1.23"
        "kyverno.io/kyverno-version"      = "1.7.0"
        "policies.kyverno.io/category"    = "Sample"
        "policies.kyverno.io/description" = "Similar to the ability to check the uniqueness of hosts and paths independently, it is possible to check for uniqueness of them both together across a cluster. This policy ensures that no Ingress can be created or updated unless it is globally unique with respect to host plus path combination."
        "policies.kyverno.io/minversion"  = "1.6.0"
        "policies.kyverno.io/severity"    = "medium"
        "policies.kyverno.io/subject"     = "Ingress"
        "policies.kyverno.io/title"       = "Unique Ingress Host and Path"
      }
      "name" = "unique-ingress-host-and-path"
    }
    "spec" = {
      "background" = false
      "rules" = [
        {
          "context" = [
            {
              "apiCall" = {
                "jmesPath" = "items[].spec.rules[]"
                "urlPath"  = "/apis/networking.k8s.io/v1/ingresses"
              }
              "name" = "rules"
            },
          ]
          "match" = {
            "any" = [
              {
                "resources" = {
                  "kinds" = [
                    "Ingress",
                  ]
                  "namespaceSelector" = local.namespace_selector
                }
              },
            ]
          }
          "name" = "check-host-path-combo"
          "preconditions" = {
            "all" = [
              {
                "key"      = "{{ request.operation || 'BACKGROUND' }}"
                "operator" = "NotEquals"
                "value"    = "DELETE"
              },
            ]
          }
          "validate" = {
            "foreach" = [
              {
                "deny" = {
                  "conditions" = {
                    "all" = [
                      {
                        "key"      = "{{ element.http.paths[].path }}"
                        "operator" = "AnyIn"
                        "value"    = "{{ rules[?host=='{{element.host}}'][].http.paths[].path }}"
                      },
                    ]
                  }
                }
                "list" = "request.object.spec.rules[]"
              },
            ]
            "message" = "The Ingress host and path combination must be unique across the cluster."
          }
        },
      ]
      "validationFailureAction" = "audit"
    }
  }
}
