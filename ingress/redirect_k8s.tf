# Redirect k8s.tardisproject.uk to the wiki page for kubernetes
resource "kubernetes_manifest" "middleware_redirect_wiki_page" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "redirect-wiki-page"
      namespace = "tardis"
    }

    spec = {
      redirectRegex = {
        regex       = "^.*"
        replacement = "https://wiki.tardisproject.uk/howto:specific:learn_k8s"
      }
    }
  }
}
resource "kubernetes_manifest" "ingressroute_redirect_k8s_tardisproject_uk" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"
    metadata = {
      name      = "redirect-k8s-tardisproject-uk"
      namespace = "tardis"
    }

    spec = {
      entryPoints = ["web", "websecure"]
      routes = [
        {
          kind  = "Rule"
          match = "Host(`k8s.tardisproject.uk`)"
          middlewares = [
            {
              name      = kubernetes_manifest.middleware_redirect_wiki_page.manifest.metadata.name
              namespace = kubernetes_manifest.middleware_redirect_wiki_page.manifest.metadata.namespace
            },
          ]
          services = [
            {
              name = "whoami"
              port = 80
            }
          ]
        }
      ]
      tls = {
        certResolver = "letsencrypt"
      }
    }
  }
}
