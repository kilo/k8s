# Limit users to creating ingresses with host: <username>.tardis.ac
resource "kubernetes_manifest" "clusterpolicy_user_ingress_restriction" {
  manifest = {
    "apiVersion" = "kyverno.io/v1"
    "kind"       = "ClusterPolicy"
    "metadata" = {
      "name" = "user-ingress-restriction"
    }
    "spec" = {
      "rules" = [
        {
          "name" = "use-user-host"
          "match" = {
            "any" = [
              {
                "resources" = {
                  "kinds" = [
                    "Ingress",
                  ]
                  "namespaceSelector" = local.namespace_selector
                }
              },
            ]
          }
          "preconditions" = {
            "all" = [
              {
                "key"      = "{{ request.operation || 'BACKGROUND' }}"
                "operator" = "AnyIn"
                "value" = [
                  "CREATE",
                  "UPDATE",
                ]
              },
            ]
          }
          "context" = [
            {
              name = "owner"
              apiCall = {
                urlPath  = "/api/v1/namespaces/{{request.namespace}}"
                jmesPath = "metadata.labels.\"tardisproject.uk/owner\""
              }
            }
          ]
          "validate" = {
            "foreach" = [
              {
                "deny" = {
                  "conditions" = {
                    "any" = [
                      {
                        "key"      = "{{element.host}}"
                        "operator" = "NotEquals"
                        "value"    = "{{owner}}.tardis.ac"
                      },
                    ]
                  }
                }
                "list" = "request.object.spec.rules"
              },
            ]
            "message" = "Host must be of form {{owner}}.tardis.ac"
          }
        },
      ]
      "validationFailureAction" = "Enforce"
    }
  }
}
