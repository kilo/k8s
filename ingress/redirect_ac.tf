# Redirect tardis.ac to tardisproject.uk
resource "kubernetes_manifest" "middleware_redirect_homepage" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "redirect-homepage"
      namespace = "tardis"
    }

    spec = {
      redirectRegex = {
        regex       = ".*"
        replacement = "https://tardisproject.uk/"
      }
    }
  }
}
resource "kubernetes_manifest" "ingressroute_redirect_tardis_ac" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"
    metadata = {
      name      = "redirect-tardis-ac"
      namespace = "tardis"
    }

    spec = {
      entryPoints = ["web", "websecure"]
      routes = [
        {
          kind  = "Rule"
          match = "Host(`tardis.ac`)"
          middlewares = [
            {
              name      = kubernetes_manifest.middleware_redirect_homepage.manifest.metadata.name
              namespace = kubernetes_manifest.middleware_redirect_homepage.manifest.metadata.namespace
            },
          ]
          services = [
            {
              name = "whoami"
              port = 80
            }
          ]
        }
      ]
      tls = {
        certResolver = "letsencrypt"
      }
    }
  }
}
