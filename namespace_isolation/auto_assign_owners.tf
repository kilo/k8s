# Auto assign the owner role when a namespace is annotated with `tardisproject.uk/owner`
resource "kubernetes_manifest" "clusterpolicy_make_user_owner" {
  manifest = {
    "apiVersion" = "kyverno.io/v1"
    "kind" = "ClusterPolicy"
    "metadata" = {
      "name" = "make-user-owner"
    }
    "spec" = {
      "background" = true
      "generateExistingOnPolicyUpdate" = true
      "mutateExistingOnPolicyUpdate" = true
      "rules" = [
        {
          "exclude" = {
            "resources" = {
              "name" = "grafana-agent"
            }
          }
          "generate" = {
            "apiVersion" = "rbac.authorization.k8s.io/v1"
            "data" = {
              "kind" = "RoleBinding"
              "roleRef" = {
                "apiGroup" = "rbac.authorization.k8s.io"
                "kind" = "ClusterRole"
                "name" = "owner"
              }
              "subjects" = [
                {
                  "apiGroup" = "rbac.authorization.k8s.io"
                  "kind" = "User"
                  "name" = "https://id.tardisproject.uk/realms/master#{{request.object.metadata.labels.\"tardisproject.uk/owner\"}}"
                },
              ]
            }
            "kind" = "RoleBinding"
            "name" = "tardis:owner-permissions"
            "namespace" = "{{request.object.metadata.name}}"
            "synchronize" = true
          }
          "match" = {
            "resources" = {
              "kinds" = [
                "Namespace",
              ]
              "selector" = {
                "matchLabels" = {
                  "tardisproject.uk/owner" = "*"
                }
              }
            }
          }
          "name" = "generate-owner-role"
        },
        {
          "exclude" = {
            "resources" = {
              "name" = "grafana-agent"
            }
          }
          "match" = {
            "resources" = {
              "kinds" = [
                "Namespace",
              ]
              "selector" = {
                "matchLabels" = {
                  "tardisproject.uk/owner" = "*"
                }
              }
            }
          }
          "mutate" = {
            "patchStrategicMerge" = {
              "metadata" = {
                "labels" = {
                  "pod-security.kubernetes.io/enforce" = "baseline"
                  "pod-security.kubernetes.io/enforce-version" = "latest"
                }
              }
            }
          }
          "name" = "add-pod-security-policy"
        },
      ]
    }
  }
}

# This is needed because kyverno cannot apply roles it is not apart of
resource "kubernetes_cluster_role_binding" "kyverno_owner" {
  metadata {
    name = "kyverno:owner"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "kyverno"
    namespace = "kyverno"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "owner"
  }
}
