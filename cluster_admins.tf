
# Binds the OIDC group 'admin' to the cluster admin role
resource "kubernetes_cluster_role_binding" "admin-group-clusterrole" {
  metadata {
    name = "admin-group-clusterrole"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "Group"
    name      = "oidc:admin"
    api_group = "rbac.authorization.k8s.io"
  }
}
