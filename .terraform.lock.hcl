# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/helm" {
  version = "2.11.0"
  hashes = [
    "h1:ohKpsfAdRWWQZpqbK99sol2Pbp7betwUKUSZksVjI3s=",
    "zh:2b505116e91325a26a0749ad9820ec4f2a9f47e195a84d6fd1f41d86b92bdefd",
    "zh:2f44100e7e6c1063a3c872066a04378b7336d5b4fbc1f6581d3e69f85dbbba3f",
    "zh:71fec2cda63d13250cf460e48e8a1e8f3200f93f15e5ed02df871fe86d6c559c",
    "zh:7a2b5c182ba8bb980a44642f475334eca8e5e689083003471d0f8272482bb209",
    "zh:9cddaf003b66530a02b2efb574ef9fecaad5a8f00ea744e435de5c6672a14a22",
    "zh:ac6cb892a2eeddae58548c9c252e266a07df7f8b894670b68729c7eae562ac92",
    "zh:ad88f8d7390ce32204b01cd74133c7ed85fbc5dab2add6c6784ce52be7a99cae",
    "zh:cc7e007ea23739ebe4efb75bf2fae2c95aa18c63ea109357d7f8f79940182f61",
    "zh:da97c83e849c74da08bb1ed70edd062db1b2469fe48fb7d44399baecba8c1009",
    "zh:f94ce933127a56d3f6019e483a1f6f5edb2255e5edfed99ecfac26e444995152",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version = "2.23.0"
  hashes = [
    "h1:vAfwUaEN5WcOXQAgH2103jNeccS+esOXDw+gGJdxbcM=",
    "zh:0493758b92d54ab8634540e2e2417fe5de956f2756e8de20f369be22bf98fb26",
    "zh:177b271f1cf02f4bbd7bed7c6a846898f66dda4751e8228017c38765f6a0f00c",
    "zh:1ce3834baf2667b1e6ba941324756a8a374e0e1eaabb5e9e204a79e8c429c228",
    "zh:347af45ce6260886ac642121201709249b53e890b3699baa418fdc9c1cbbd33a",
    "zh:4eaf5f965c4c1ea0eddfde6ca718400def8c8d9c517d4689520a776f54e48511",
    "zh:541b89dbe56f07707e44bfedc36a9e5471b6ddca84f90e493dc18e2101a2e363",
    "zh:59b32de1d4bf7fb31a00675bf51e7d98e8243e5cf7e0e84c49768c70c7f70631",
    "zh:8948caac6323b3f379943671c5b9dd77749de68fed6d67fbac79c1f5b9dea7bf",
    "zh:9cb44cfc0448a62a3e473cd5d230d3a66ff0cd1bc37047b333740649a9797e43",
    "zh:c14f7bb31a31cf600678ab80885cfb263068ec8db5a2fdded082c7e05c6bff5c",
  ]
}
