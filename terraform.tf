terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    helm = {
      source = "hashicorp/helm"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "tardis-admin"
}

provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = "tardis-admin"
  }
}

module "namespace_isolation" {
  source = "./namespace_isolation/"
}

module "ingress" {
  source = "./ingress/"
}

terraform {
  backend "kubernetes" {
    secret_suffix = "state"
    namespace     = "tardis"
    config_path   = "~/.kube/config"
  }
}
