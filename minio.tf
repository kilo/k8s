# Deploys minio object storage to the cluster, in single-node single-disk configuration
resource "kubernetes_namespace" "minio" {
  metadata {
    name = "minio"
  }
}

resource "kubernetes_deployment_v1" "minio" {
  metadata {
    name      = "minio"
    namespace = "minio"
    labels = {
      app = "minio"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "minio"
      }
    }

    template {
      metadata {
        labels = {
          app = "minio"
        }
      }

      spec {
        container {
          name  = "minio"
          image = "quay.io/minio/minio:latest"

          command = [
            "/bin/bash",
            "-c",
            "minio server /data --console-address :9090",
          ]

          env {
            name  = "MINIO_BROWSER_REDIRECT_URL"
            value = "https://minio-console.on.tardis.ac/oauth_callback"
          }

          env_from {
            secret_ref {
              name = "minio-root-pw"
            }
          }

          volume_mount {
            name       = "data"
            mount_path = "/data"
          }
        }

        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = "minio-data"
          }
        }
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "minio-data" {
  metadata {
    name      = "minio-data"
    namespace = "minio"
    labels = {
      app = "minio"
    }
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "10Gi"
      }
    }
  }
}

resource "kubernetes_service_v1" "minio" {
  metadata {
    name      = "minio"
    namespace = "minio"
    labels = {
      app = "minio"
    }
  }

  spec {
    type = "LoadBalancer"
    selector = {
      app = "minio"
    }

    port {
      name        = "s3"
      port        = "9000"
      target_port = "9000"
    }

    port {
      name        = "dashboard"
      port        = "9090"
      target_port = "9090"
    }
  }
}

resource "kubernetes_ingress_v1" "minio-s3" {
  metadata {
    name      = "minio-s3"
    namespace = "minio"
    annotations = {
      "traefik.ingress.kubernetes.io/router.priority" : "1"
      "traefik.ingress.kubernetes.io/router.tls" : "true"
      "traefik.ingress.kubernetes.io/router.tls.certresolver"   = "letsencrypt"
      "traefik.ingress.kubernetes.io/router.tls.domains.0.main" = "*.on.tardis.ac"
    }
  }

  spec {
    rule {
      host = "minio.on.tardis.ac"
      http {
        path {
          path = "/"

          backend {
            service {
              name = "minio"
              port {
                number = 9000
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress_v1" "minio-console" {
  metadata {
    name      = "minio-console"
    namespace = "minio"
    annotations = {
      "traefik.ingress.kubernetes.io/router.priority" : "1"
      "traefik.ingress.kubernetes.io/router.tls" : "true"
      "traefik.ingress.kubernetes.io/router.tls.certresolver"   = "letsencrypt"
      "traefik.ingress.kubernetes.io/router.tls.domains.0.main" = "*.on.tardis.ac"
    }
  }

  spec {
    rule {
      host = "minio-console.on.tardis.ac"
      http {
        path {
          path = "/"

          backend {
            service {
              name = "minio"
              port {
                number = 9090
              }
            }
          }
        }
      }
    }
  }
}
