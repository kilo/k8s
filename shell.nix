{pkgs ? import <nixpkgs> {}}:
pkgs.mkShell {
  buildInputs = with pkgs; [
    k2tf
    tfk8s
    k0sctl
    kubectl
    kyverno
    terraform
    kubernetes-helm
  ];
}
